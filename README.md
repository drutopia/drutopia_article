<!-- writeme -->
Drutopia Article
================

Drutopia Article is a base feature providing an article content type and related configuration.

 * https://gitlab.com/drutopia/drutopia_article
 * Package name: drupal/drutopia_article


### Requirements

 * drupal/block_visibility_groups ^1.3
 * drupal/config_actions ^1.1
 * drupal/ctools ^3.4
 * drupal/drutopia_seo ^1.0
 * drupal/ds ^3.7
 * drupal/facets ^2
 * drupal/field_group ^3.0
 * drupal/pathauto ^1.8
 * drupal/token ^1.7


### License

GPL-2.0+

<!-- endwriteme -->

### Planned Features
* Relate an article to an action, campaign or group.
* Post an article to Facebook or Twitter to your account when publishing/updating.

### Potential Future Features
* Collaborative, moderated editing of an article (think wiki meets blog)
* SMS integration

